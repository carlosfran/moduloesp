#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

const char* ssid = "";
const char* password = "";

ESP8266WebServer server(80);

const int led = D2;
int led_state = LOW;

int ldr = A0;

void indexPage() {
  server.send(200, "application/json", "hello!");
}

void ledON(){
  digitalWrite(led, HIGH);
  led_state = HIGH;
  int lum = analogRead(ldr);
  String page = "{\"led\": "+String(led_state);
  page += ", \"ldr\": ";
  page += String(lum);
  page += "}";
  server.send(200, "application/json", page);
}

void ledOFF(){
  digitalWrite(led, LOW);
  led_state = LOW;
  int lum = analogRead(ldr);
  String page = "{\"led\": "+String(led_state);
  page += ", \"ldr\": ";
  page += String(lum);
  page += "}";
  server.send(200, "application/json", page);
}

void ledState(){
  int lum = analogRead(ldr);
  String page = "{\"led\": "+String(led_state);
  page += ", \"ldr\": ";
  page += String(lum);
  page += "}";
  server.send(200, "application/json", page);
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i = 0; i < server.args(); i++) {
    message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
  }
  server.send(404, "application/json", message);
}

void setup(void) {
  
  Serial.begin(115200);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  Serial.println("");
  
  pinMode(led, OUTPUT);
  pinMode(ldr, INPUT);
  
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(ssid);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("LEDWebServer")) {
    Serial.println("MDNS responder started");
  }
  
  server.on("/", indexPage);
  
  server.on("/led/state", ledState);

  server.on("/led/on", ledON);

  server.on("/led/off", ledOFF);

  server.on("/inline", []() {
    server.send(200, "application/json", "this works as well");
  });

  server.onNotFound(handleNotFound);

  server.begin();
  Serial.println("HTTP server started");
}

void loop(void) {
  server.handleClient();
}
